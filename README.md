A Debian repository containing Free Software not included in PureOS Amber. It currently supports amd64 and arm64.

Repository should be compatible with Debian 10 (Buster).

# Usage

`wget https://polarisfm.net/gpg.txt`

`sudo apt-key add gpg.txt`

`sudo echo "deb https://codeberg.org/polarisfm/debian-repo/raw/branch/master/debian/ amber main" > /etc/apt/sources.list.d/polarisfm.list`

`sudo apt update`

# Packages

Please see 'packagelist.txt' for a full list of packages. This list is signed with my GPG key.

# How can I trust these?

Anything labelled "repack" is repacked from the developer's binaries. Trust them as much as you would trust those binaries.

Things not labelled "repack" are sourced from either the project's official Debain package or a newer version of Debian (ex. Debain Buster or Debian Sid)

Every commit is signed with my GPG key and pushing is done through RSA. Additionally the Debian repository is signed with my GPG key.

A list of where everything is sourced from is avaliable at packagesources.txt. It is signed by my GPG key.

I also have a canary (as part of the Onion Mirroring Guidelines spec): https://polarisfm.net/canary.txt

It should be updated every 14 days with the date and the latest (at the time of signing) Bitcoin blockchain hash.
